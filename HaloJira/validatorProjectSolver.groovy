import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.project.version.Version

String fieldNameProject = "Проект JIRA"
String fieldNameVersionAuto = "Проявляется в"
String fieldNameVersionManual = "Проявляется в версии"

def versionManager = ComponentAccessor.getVersionManager()

if (issue.projectId.equals(cfValues[fieldNameProject]?.id)) {
    def versionsValue = cfValues[fieldNameVersionAuto];
    if (versionsValue != null) {
        return true
    }
}
else {
    String versionsValue = cfValues[fieldNameVersionManual]
    if (versionsValue != null) {
        String versionString = (String) versionsValue
        if (versionString != null) {
            Version versionFound = versionManager.getVersion(cfValues[fieldNameProject]?.id, versionString)
            if (versionFound != null) {
                return true
            }
        }
    }
}
return false