def checkboxesField = getFieldByName("Типы размещаемых работ")
def timeField1 = getFieldByName("Время на тестирование")
def timeField2 = getFieldByName("Время на документирование")
def timeField3 = getFieldByName("Время на чек-листы")
def timeField4 = getFieldByName("Время на автотесты")
def timeField5 = getFieldByName("Время на РТП")

String[] timeFieldNames = [ "Время на тестирование",
                            "Время на документирование",
                            "Время на чек-листы",
                            "Время на автотесты",
                            "Время на РТП"] as String[]

String[] types = [ "Тестирование", "Документирование",
                   "Чек-листы", "Автотесты", "РТП" ] as String[]

int amount = 5

def markedTypesFiller = "-filler-" // без этого не отрабатывал корректно
def markedTypes = markedTypesFiller + ((String)checkboxesField.getValue())

for (int i = 0; i < amount; i++) {
    SetEditableIfChecked(markedTypes,
            timeFieldNames[i],
            types[i])
}



void SetEditableIfChecked(
        String checkedTypes,
        String timeFieldName,
        String typeName)
{
    def timeField = getFieldByName(timeFieldName)
    if (checkedTypes.contains(typeName)) {
        timeField.setReadOnly(false);
    }
    else  {
        timeField.setReadOnly(true);
    }
}