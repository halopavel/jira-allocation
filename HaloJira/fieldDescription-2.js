﻿<script type="text/javascript">

	// Получаем узлы значений чекбоксов и соответствующих полей "Время на ...".
	// Изменился порядок чек-боксов -- появился хаос в названиях переменных.

	chboxBase = 'customfield_14522-'
	chboxRtp = '1';
	chboxCheckLists = '2';
	chboxTesting = '3';
	chboxDocumenting = '4';
	chboxAutotests = '5';

	idRtp = 'customfield_17320';
	idCheckLists = 'customfield_14525';
	idTesting = 'customfield_14523';
	idDocumenting = 'customfield_14524';
	idAutotests = 'customfield_14526s';

	checkbox1 = document.getElementById(chboxBase + chboxRtp);
	time1 = document.getElementById(idRtp);

	checkbox2 = document.getElementById(chboxBase + chboxCheckLists);
	time2 = document.getElementById(idCheckLists);

	checkbox3 = document.getElementById(chboxBase + chboxTesting);
	time3 = document.getElementById(idTesting);

	checkbox4 = document.getElementById(chboxBase + chboxDocumenting);
	time4 = document.getElementById(idDocumenting);

	checkbox5 = document.getElementById(chboxBase + chboxAutotests);
	time5 = document.getElementById(idAutotests);

	// Формируем узел с подписями - перед полем "Время на ...".
	textBefore = document.createElement('span');
	textBefore.innerHTML = '<br>Оценка:  ';
	// Формируем узел с подписями - после поля "Время на ...".
	textAfter = document.createElement('span');
	textAfter.innerHTML = "<span class='aui-form example'>  (например, 3w 4d 12h)  </span><a class='help-lnk' href='/secure/ShowTimeTrackingHelp.jspa?decorator=popup#TimeTracking' title='Get local help' data-helplink='local'><span class='aui-icon aui-icon-small aui-iconfont-help'></span>";

	/**
	 * Для каждого значения чекбокса:
	 * - скрываем подпись "Время на ..."
	 * - добавляем подпись перед полем 
	 * - добавляем само поле
	 * - добавляем подпись после поля
	 * - "укорачиваем" поле
	 */
	time1.parentNode.style.display = 'none';
	checkbox1.parentNode.appendChild(textBefore.cloneNode(true));
	checkbox1.parentNode.appendChild(time1);
	checkbox1.parentNode.appendChild(textAfter.cloneNode(true));
	time1.setAttribute('class','text short-field');

	time2.parentNode.style.display = 'none';
	checkbox2.parentNode.appendChild(textBefore.cloneNode(true));
	checkbox2.parentNode.appendChild(time2);
	checkbox2.parentNode.appendChild(textAfter.cloneNode(true));
	time2.setAttribute('class','text short-field');

	time3.parentNode.style.display = 'none';
	checkbox3.parentNode.appendChild(textBefore.cloneNode(true));
	checkbox3.parentNode.appendChild(time3);
	checkbox3.parentNode.appendChild(textAfter.cloneNode(true));
	time3.setAttribute('class','text short-field');

	time4.parentNode.style.display = 'none';
	checkbox4.parentNode.appendChild(textBefore.cloneNode(true));
	checkbox4.parentNode.appendChild(time4);
	checkbox4.parentNode.appendChild(textAfter.cloneNode(true));
	time4.setAttribute('class','text short-field');

	time5.parentNode.style.display = 'none';
	checkbox5.parentNode.appendChild(textBefore.cloneNode(true));
	checkbox5.parentNode.appendChild(time5);
	checkbox5.parentNode.appendChild(textAfter.cloneNode(true));
	time5.setAttribute('class','text short-field');

	// Скрываем поле комментария.
	document.getElementsByClassName('field-group aui-field-wikiedit comment-input')[1].style.display = 'none';

	// Поля "Время на ..." закрываем от редактирования.
	time1.setAttribute('class', 'text short-field clientreadonly');
	time1.setAttribute('disabled', 'disabled');
	time2.setAttribute('class', 'text short-field clientreadonly');
	time2.setAttribute('disabled', 'disabled');
	time3.setAttribute('class', 'text short-field clientreadonly');
	time3.setAttribute('disabled', 'disabled');
	time4.setAttribute('class', 'text short-field clientreadonly');
	time4.setAttribute('disabled', 'disabled');
	time5.setAttribute('class', 'text short-field clientreadonly');
	time5.setAttribute('disabled', 'disabled');

</script>
