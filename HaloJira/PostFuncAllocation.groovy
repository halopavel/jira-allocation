import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.PriorityManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.Field
import com.atlassian.jira.issue.fields.layout.field.FieldLayout
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager
import com.atlassian.jira.issue.index.IndexException
import com.atlassian.jira.issue.issuetype.IssueType
import com.atlassian.jira.issue.label.LabelManager
import com.atlassian.jira.issue.link.IssueLinkType
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.priority.Priority
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder
import com.atlassian.jira.project.Project
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem
import com.atlassian.jira.project.version.Version
import com.atlassian.jira.project.version.VersionManager
import com.atlassian.jira.util.ImportUtils
import com.atlassian.jira.util.JiraDurationUtils
import com.atlassian.jira.issue.link.RemoteIssueLinkBuilder
import com.atlassian.jira.bc.issue.link.RemoteIssueLinkService
import com.atlassian.jira.issue.link.RemoteIssueLink
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.codehaus.groovy.runtime.typehandling.GroovyCastException
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler

log = Logger.getLogger("com.acme.CreateSubtask")
log.setLevel(Level.DEBUG)

log.info "__________OTD-ALLOCATION: POSTFUNCTION STARTED___________"


/**
 * Настройка параметров, зависящих от версий, языка и т.д.
 */
class cs {
    // Названия полей.
    static String fieldNameReworkTitle = "Название доработки"
    static String fieldNameLinkToCurrentIssue = "Связь с текущим запросом"
    static String fieldNameAllocationTypes = "Типы размещаемых работ"
    static String fieldNameProject = "Проект JIRA"
    static String fieldNameRelease = "Релиз"
    static String fieldNameVersionAuto = "Проявляется в"
    static String fieldNameVersionManual = "Проявляется в версии"
    static String fieldNamePercentDone = "PercentDone"
    static String fieldNameDevIssue = "Номер запроса разработки"
    static String fieldNameEstimateIssue = "Номер запроса на оценку"
    static String fieldNameRootIssue = "Корневой запрос для размещения"
    static String fieldNameTRProjectName = "Проект в TestRail (название)"
    static String fieldNameTRProjectLink = "Проект в TestRail (ссылка)"
    static String fieldNameTRMilestoneName = "Майлстон в TestRail (название)"
    static String fieldNameTRMilestoneLink = "Майлстон в TestRail (ссылка)"
    static String fieldNameConfLinkRequire = "Ссылка на требования"
    static String fieldNameConfLinkTestingPage = "Ссылка на сводную страницу по тестированию"
    // Типы работ.
    static String typeTesting = "Тестирование"
    static String typeDocument = "Документирование"
    static String typeChecklist = "Чек-листы"
    static String typeAutotest = "Автотесты"
    static String typeRTP = "РТП"
    // Связи.
    static String linkTypeBlocks = "блок"
    static String linkTypeSeeAlso = "см. также"
    // Тип запроса.
    static String issueTypeEpic = "Epic"
    static String issueTypeTask = "Task"
    // Приоритет.
    static String priorityMajor = "Major"
}

def customFieldManager = ComponentAccessor.getCustomFieldManager()


// Получаем значения "Типы размещаемых работ" (чекбокс).
def allocTypesField = customFieldManager.getCustomFieldObjectByName(cs.fieldNameAllocationTypes)
String allocationTypes = allocTypesField.getValue(issue)
if (allocationTypes == null || "".equals(allocationTypes)) {
    log.info "OTD-ALLOCATION. Issue types not selected."
    return
}
log.info "OTD-ALLOCATION. Selected issue types to create: " + allocationTypes


/**
 * Создаём запросы в зависимости от отмеченных типов.
 */
Issue testingIssue = null
Issue documentIssue = null
Issue checklistIssue = null
Issue autotestIssue = null
Issue rtpIssue = null

// Полученные значения содержат "Тестирование".
if (allocationTypes.contains(cs.typeTesting))
    testingIssue = createIssueByParameters(cs.typeTesting)
// Полученные значения содержат "Документирование".
if (allocationTypes.contains(cs.typeDocument))
    documentIssue = createIssueByParameters(cs.typeDocument)
// Полученные значения содержат "Чек-листы".
if (allocationTypes.contains(cs.typeChecklist))
    checklistIssue = createIssueByParameters(cs.typeChecklist)
// Полученные значения содержат "Автотесты".
if (allocationTypes.contains(cs.typeAutotest))
    autotestIssue = createIssueByParameters(cs.typeAutotest)
// Полученные значения содержат "РТП".
if (allocationTypes.contains(cs.typeRTP))
    rtpIssue = createIssueByParameters(cs.typeRTP)

// Формирование связей между запросами.
String isLinkToCurrentIssue = customFieldManager.getCustomFieldObjectByName(cs.fieldNameLinkToCurrentIssue).getValue(issue)
if (isLinkToCurrentIssue != null && !"".equals(isLinkToCurrentIssue)) {
    log.info "OTD-ALLOCATION. Link to current issue is enabled."
    createLink(issue, testingIssue, cs.linkTypeSeeAlso)
    createLink(issue, documentIssue, cs.linkTypeSeeAlso)
    createLink(issue, checklistIssue, cs.linkTypeSeeAlso)
    createLink(issue, autotestIssue, cs.linkTypeSeeAlso)
    createLink(issue, rtpIssue, cs.linkTypeSeeAlso)
}

createLink(testingIssue, documentIssue, cs.linkTypeBlocks)
createLink(testingIssue, autotestIssue, cs.linkTypeBlocks)
createLink(checklistIssue, testingIssue, cs.linkTypeBlocks)
createLink(documentIssue, autotestIssue, cs.linkTypeBlocks)

// Связь с запросом из "Номер запроса на оценку"
createLink(cs.fieldNameEstimateIssue, null, checklistIssue, cs.linkTypeSeeAlso)

// Связь с запросом из "Номер запроса разработки"
createLink(cs.fieldNameDevIssue, null, testingIssue, cs.linkTypeBlocks)

// Периодически не отображаются связи, метки; при этом в истории указывается, что они создавались
reIndexIssue(testingIssue);
reIndexIssue(documentIssue);
reIndexIssue(checklistIssue);
reIndexIssue(autotestIssue);
reIndexIssue(rtpIssue)

/**
 * Метод создания Issue по передаваемым параметрам.
 *
 * @param type
 * отмеченный в чекбоксе тип
 * @param cs
 * параметры
 * @return {@link Issue} созданный запрос
 */
private Issue createIssueByParameters(String type) {

    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def issueMgr = ComponentManager.getInstance().getIssueManager()
    def projectMgr = ComponentManager.getInstance().getProjectManager()
    def issueFactory = ComponentManager.getInstance().getIssueFactory()
    def userManager = ComponentAccessor.getUserManager()
    def currentUser = ComponentAccessor.getJiraAuthenticationContext().getUser().getDirectoryUser()
    LabelManager labelManager = ComponentAccessor.getComponent(LabelManager.class)
    def versionManager = ComponentAccessor.getVersionManager()

    // Проект.
    String projectKey = null
    if ( cs.typeAutotest.equals(type) ) {
        projectKey = "CMJTES"
    }
    else {
        projectKey = customFieldManager.getCustomFieldObjectByName(cs.fieldNameProject).getValue(issue).get("key").toString()
        log.info "OTD-ALLOCATION. NEW ISSUE. Project Key: " + projectKey
    }
    Project project = projectMgr.getProjectByCurrentKey(projectKey)
    log.info "OTD-ALLOCATION. NEW ISSUE. Project Name: " + project.name

    // Тема.
    def release = customFieldManager.getCustomFieldObjectByName(cs.fieldNameRelease).getValue(issue)
    def reworkTitle = customFieldManager.getCustomFieldObjectByName(cs.fieldNameReworkTitle).getValue(issue)
    String typeForTheme = (type.equals(cs.typeChecklist) || type.equals(cs.typeAutotest)) ?
            type.substring(0, type.length() - 1) : type
    String summary = typeForTheme + ". " + release + ". " + reworkTitle
    log.info "OTD-ALLOCATION. NEW ISSUE. Summary: " + summary

    // Тип запроса.
    String issueTypeId = -1
    if ( cs.typeTesting.equals(type) ) {
        issueTypeId = getIssueTypeIdByName(project, cs.issueTypeEpic)
    }
    else {
        issueTypeId = getIssueTypeIdByName(project, cs.issueTypeTask)
    }
    log.info "OTD-ALLOCATION. NEW ISSUE. Issue Type ID: " + issueTypeId

    // Приоритет.
    def priorityId = getPriorityIdByName(cs.priorityMajor)
    log.info "OTD-ALLOCATION. NEW ISSUE. Priority: " + priorityId

    // Исполнитель.
    def assignee = userManager.getUserByName("otd_RTP").directoryUser
    log.info "OTD-ALLOCATION. NEW ISSUE. Assignee: " + assignee

    // Первоначальная оценка.
    String timeFieldTitle = "Время на " + (cs.typeRTP.equals(type) ? type : type.toLowerCase())
    String estimateInStr = customFieldManager.getCustomFieldObjectByName(timeFieldTitle).getValue(issue).toString()
    Long estimateLong = -1;
    if ( estimateInStr != null && !"".equals(estimateInStr) ) {
        try {
            estimateLong = ComponentAccessor.jiraDurationUtils.parseDuration(estimateInStr,
                    ComponentAccessor.getJiraAuthenticationContext().getLocale())
        }
        catch (Exception e) {
            log.error "OTD-ALLOCATION. Original Estimate: wrong input string: " + estimateInStr
            estimateLong = 0;
        }
    }
    log.info "OTD-ALLOCATION. NEW ISSUE. Original Estimate: " + estimateInStr + " [" + estimateLong.toString() + "]";


    // Описание.
    String description = ''
    if ( cs.typeTesting.equals(type) ) {
        description = "||Тест-раны исправления: текущие||Тест-раны исправления: завершены||Отчёты||\n" +
                "|[Тест-ран|]| | |"
    } else if ( cs.typeChecklist.equals(type)) {
        def estimateIssue = customFieldManager.getCustomFieldObjectByName(cs.fieldNameEstimateIssue).getValue(issue)
        String estimateIssueString = ""
        if (estimateIssue instanceof List) {
            boolean isFirst = true;
            for (def elem : (List) estimateIssue) {
                if (!isFirst) {
                    estimateIssueString += ", "
                }
                isFirst = false
                estimateIssueString += elem.toString()
            }
        } else if (estimateIssue != null) {
            estimateIssueString = estimateIssue.toString();
        }

        def testRailName = customFieldManager.getCustomFieldObjectByName(cs.fieldNameTRProjectName).getValue(issue)
        def testRailLink = customFieldManager.getCustomFieldObjectByName(cs.fieldNameTRProjectLink).getValue(issue)
        def testRailMlstnName = customFieldManager.getCustomFieldObjectByName(cs.fieldNameTRMilestoneName).getValue(issue)
        def testRailMlstnLink = customFieldManager.getCustomFieldObjectByName(cs.fieldNameTRMilestoneLink).getValue(issue)

        description = "Оценка давалась в рамках " + estimateIssueString + ".\n" +
            "Тест-сьют делать в проекте [" + (testRailName == null ? "" : testRailName) + "|" +
                (testRailLink == null ? "" : testRailLink) + "].\n" +
            "Тест-ран стартовать без исполнителя в майлстоне [" + (testRailMlstnName == null ? "" : testRailMlstnName) +
                "|" + (testRailMlstnLink == null ? "" : testRailMlstnLink) + "]."
    }
    log.info "OTD-ALLOCATION. NEW ISSUE. Description: " + description

    // Проявляется в версиях.
    Collection<Version> versions = null
    if (!cs.typeAutotest.equals(type)) {
        Long projectToCreateId = customFieldManager.getCustomFieldObjectByName(cs.fieldNameProject).getValue(issue).id
        log.info projectToCreateId
        if (issue.projectId.equals(projectToCreateId)) {
            versions = (Collection<Version>) customFieldManager.getCustomFieldObjectByName(cs.fieldNameVersionAuto).getValue(issue)
            log.info "OTD-ALLOCATION. NEW ISSUE. VERSIONS (SELECTABLE): " + versions
        } else {
            String versionString = (String) customFieldManager.
                    getCustomFieldObjectByName(cs.fieldNameVersionManual).getValue(issue)
            if (versionString != null) {
                def versionFound = versionManager.getVersion(projectToCreateId, versionString)
                log.info projectToCreateId + " " + versionString + " " + versionFound
                if (versionFound != null) {
                    versions = new ArrayList<Version>()
                    versions.add(versionFound)
                }
            }
            log.info "OTD-ALLOCATION. NEW ISSUE. VERSIONS (MANUAL INPUT): " + versions
        }
    } else {
        log.info "OTD-ALLOCATION. NEW ISSUE. VERSIONS SKIPPED"
    }


    // Формирование параметров.
    def newissue = issueFactory.getIssue()
    newissue.setSummary(summary)
    newissue.setProject(projectMgr.getProjectByKey(project.key))
    newissue.setIssueTypeId (issueTypeId)
    newissue.setPriorityId(priorityId)
    newissue.setReporter(currentUser)
    newissue.setAssignee(assignee)
    newissue.setDescription(description)
    newissue.setOriginalEstimate(estimateLong)
    newissue.setEstimate(estimateLong)
    if (versions != null)
        newissue.setAffectedVersions(versions)

    // Создание запроса.
    def params = ["issue":newissue]
    Issue newIssue = issueMgr.createIssueObject(currentUser, params)
    log.info "OTD-ALLOCATION. Issue Created: '" + newissue.getKey() + "'"


    // Epic Name = Тема.
    if (cs.typeTesting.equals(type)) {
        ChangeCustomFieldValue("Epic Name", customFieldManager, newIssue, summary)
        log.info "OTD-ALLOCATION. NEW ISSUE. Epic Name: " + summary
    }

    // PercentDone.
    if (cs.typeTesting.equals(type)) {
        double zero = 0.0
        ChangeCustomFieldValue(cs.fieldNamePercentDone, customFieldManager, newIssue, zero)
        log.info "OTD-ALLOCATION. NEW ISSUE. PercentDone: " + customFieldManager.getCustomFieldObjectByName(cs.fieldNamePercentDone).getValue(newIssue)
    }

    // Метки.
    Set<String> labels = new HashSet<String>();
    labels.add(release.toString().replace(' ', '_'));
    String label2 = null
    switch (type) {
        case cs.typeTesting:
            label2 = "Задача_на_тестирование"
            break;
        case cs.typeDocument:
            label2 = "Документирование"
            break;
        case cs.typeChecklist:
            label2 = "Чек-лист"
            break;
        case cs.typeRTP:
            label2 = "РТП"
            break;
        default:
            break;
    }
    if (label2 != null)
        labels.add(label2);
    labelManager.setLabels(currentUser, newIssue.id, labels, false, false)
    log.info "OTD-ALLOCATION. NEW ISSUE. Labels (set): " + labels.toString()

    // Ссылка на требования
    try {
        if (!cs.typeRTP.equals(type)) {
            def confLinkReqField = customFieldManager.getCustomFieldObjectByName(cs.fieldNameConfLinkRequire)
            String confLinkReq = confLinkReqField.getValue(issue)
            if (confLinkReq != null && !"".equals(confLinkReq)) {
                createConfluenceLink(newIssue, confLinkReq)
            }
            log.info "OTD-ALLOCATION. NEW ISSUE. CONFLUENCE LINK TO REQUIREMENTS PROCESSED"
        }
    } catch (Exception e) {
        log.error("OTD-ALLOCATION. UNABLE TO SET CONLUENCE LINK TO REQUIREMENTS", e)
    }

    // Ссылка на сводную страницу по тестированию.
    try {
        def confLinkTestPageField = customFieldManager.getCustomFieldObjectByName(cs.fieldNameConfLinkTestingPage)
        String confLinkTestPage = confLinkTestPageField.getValue(issue)
        if (confLinkTestPage != null && !"".equals(confLinkTestPage)) {
            createConfluenceLink(newIssue, confLinkTestPage)
        }
        log.info "OTD-ALLOCATION. NEW ISSUE. CONFLUENCE LINK TO TESTING PAGE PROCESSED"
    } catch (Exception e) {
        log.error("OTD-ALLOCATION. UNABLE TO SET CONLUENCE LINK TO TESTING PAGE", e)
    }

    // Epic Link
    if (!cs.typeTesting.equals(type)) {
        try {
            List<String> rootIssues =
                    (List<String>) customFieldManager.getCustomFieldObjectByName(cs.fieldNameRootIssue).getValue(issue)
            if (rootIssues != null && !rootIssues.isEmpty()) {
                def rootIssue = issueMgr.getIssueObject(rootIssues.get(0))
                if (rootIssue != null) {
                    ChangeCustomFieldValue("Epic Link", customFieldManager, newIssue, rootIssue)
                    log.info "OTD-ALLOCATION. NEW ISSUE. EPIC LINK SET"
                }
            }
        } catch (Exception e) {
            log.error("OTD-ALLOCATION. UNABLE TO SET EPIC LINK", e)
        }
    }

    log.info "OTD-ALLOCATION. NEW ISSUE. RETURN"
    return newIssue
}


/**
 * Возвращает ID типа запроса по названию.
 *
 * @param project
 * @param nameIssueType
 * @return {@link String}
 */
private String getIssueTypeIdByName(Project project, String nameIssueType) {
    projectMgr = ComponentManager.getInstance().getProjectManager()
    Collection<IssueType> issueTypesForProject = ComponentAccessor.getIssueTypeSchemeManager().getIssueTypesForProject(project);
    for (IssueType issueType : issueTypesForProject) {
        if (issueType.getName().equals(nameIssueType)) {
            return issueType.getId();
        }
    }
    return "";
}


/**
 * Возвращает ID связи по названию.
 *
 * @param linkTypeName
 * @return {Long}
 */
private Long getLinkTypeIdByName(String linkTypeName) {
    Collection<IssueLinkType> issueLinkTypes =
            ((IssueLinkTypeManager) ComponentManager.
                    getComponentInstanceOfType(IssueLinkTypeManager.class)).getIssueLinkTypes()
    for (IssueLinkType linkType : issueLinkTypes) {
        if(linkType.getName().equals(linkTypeName))
            return linkType.getId();
    }
    return -1;
}


/**
 * Возвращает ID приоритета по названию.
 *
 * @param typeName
 * @return
 */
private String getPriorityIdByName(String typeName) {
    List<Priority> priorityTypes =
            ((PriorityManager) ComponentManager.
                    getComponentInstanceOfType(PriorityManager.class)).getPriorities()
    for (Priority priorityType : priorityTypes) {
        if(priorityType.getName().equals(typeName))
            return priorityType.getId()
    }
    return -1;
}


/**
 * Изменяет значение custom-поля.
 *
 * @param cfName
 * @param customFieldManager
 * @param anIssue
 * @param newValue
 */
public void ChangeCustomFieldValue(String cfName, CustomFieldManager customFieldManager, Issue anIssue, Object newValue) {
    CustomField cf = customFieldManager.getCustomFieldObjectByName(cfName);
    FieldLayoutItem flo = getFieldLayoutItem (anIssue, cf);
    ModifiedValue modifiedValue = new ModifiedValue(anIssue.getCustomFieldValue(cf), newValue);
    ((MutableIssue)anIssue).setCustomFieldValue(cf, modifiedValue);
    IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
    cf.updateValue(flo, anIssue, modifiedValue, changeHolder);
}


/**
 * Используется в методе изменения значения custom-поля.
 *
 * @param issue
 * @param field
 * @return
 */
public static FieldLayoutItem getFieldLayoutItem(Issue issue, Field field) {
    final FieldLayoutManager fieldLayoutManager = ComponentManager.getInstance().getFieldLayoutManager();

    FieldLayout layout = fieldLayoutManager.getFieldLayout(
            issue.getProjectObject().getGenericValue(),
            issue.getIssueTypeObject().getId()
    );

    if (layout.getId() == null) {
        layout = fieldLayoutManager.getEditableDefaultFieldLayout();
    }

    return layout.getFieldLayoutItem(field.getId());
}

private void createLink(final String fieldName, final Issue fromIssue, final Issue toIssue, final String linkType) {

    if (fromIssue == null && toIssue == null)
        return

    def customFieldManager = ComponentAccessor.getCustomFieldManager()

    try {
        List<String> issues =
                (List<String>) customFieldManager.getCustomFieldObjectByName(fieldName).getValue(issue)
        if (issues != null && !issues.isEmpty()) {
            def issueMgr = ComponentManager.getInstance().getIssueManager()
            for (String iss : issues) {
                if (fromIssue == null) {
                    createLink(issueMgr.getIssueObject(iss), toIssue, linkType)
                } else {
                    createLink(fromIssue, issueMgr.getIssueObject(iss), linkType)
                }
                log.info "OTD-ALLOCATION. Link to issue from field " + fieldName + ": " + iss
            }
        }
    } catch (GroovyCastException | NullPointerException e) {
        log.error("OTD-ALLOCATION. Error while creating link to issue from field " + fieldName + ": " + e.toString());
    }

}

/**
 * Создание связи между двумя запросами.
 *
 * @param fromIssue
 * @param toIssue
 * @param linkType
 */
private void createLink(Issue fromIssue, Issue toIssue, String linkType) {

    def issueLinkManager = ComponentAccessor.getIssueLinkManager()
    def currentUser = ComponentAccessor.getJiraAuthenticationContext().getUser().getDirectoryUser()

    if (fromIssue != null && toIssue != null)
        issueLinkManager.createIssueLink(fromIssue.id, toIssue.id, getLinkTypeIdByName(linkType), 0, currentUser)
}

/**
 * Выполнение re-index для issue
 *
 * @param issue - если null, просто выход
 */
private void reIndexIssue(Issue issue){
    if (issue == null)
        return

    boolean wasIndexing = ImportUtils.isIndexIssues();
    ImportUtils.setIndexIssues(true);
    try {
        ComponentAccessor.getIssueIndexManager().reIndex(issue);
        log.info "OTD-ALLOCATION. Issue re-indexed: " + issue.getKey()
    } catch (IndexException e) {
        System.out.println("OTD-ALLOCATION. Re-index error.");
    } finally{
        ImportUtils.setIndexIssues(wasIndexing);
    }
}

/**
 * Создаёт ссылку на страницу Confluence. Название ссылки получает через REST-API Confluence.
 * @param issue - запрос, в котором добавляется ссылка
 * @param confUrl - URL ссылки
 */
private void createConfluenceLink(final Issue issue, final String confUrl) {

    log.info "OTD-ALLOCATION. CONFLUENCE LINK. FOR ISSUE " + issue.getKey()

    if (confUrl == null || confUrl.isEmpty()) {
        log.error "OTD-ALLOCATION. CONFLUNECE LINK. EMPTY URL!"
        return
    }

    // Получение id страницы (реализация "в лоб").
    log.info "OTD-ALLOCATION. CONFLUENCE LINK. TRY TO GET PAGE ID FOR URL " + confUrl
    String confPageId = ""
    String[] urlParts = confUrl.split("pageId=")
    if (urlParts.length > 1) {
        StringBuilder sb = new StringBuilder()
        char[] charArr = urlParts[1].toCharArray()
        for (int i = 0; i < charArr.length; i++) {
            if (Character.isDigit(charArr[i])) {
                sb.append(charArr[i]);
            } else {
                break;
            }
        }
        confPageId = sb.toString();
        log.info "OTD-ALLOCATION. CONFLUENCE LINK. PAGE ID: " + confPageId
    } else {
        log.warn "OTD-ALLOCATION. CONFLUENCE LINK. UNABLE TO GET ID FROM URL " + confUrl
        return
    }

    // Для получения названия страницы нужно сделать REST-запрос к Confluence.
    String confRestUrl = "rest/prototype/1/content/" + confPageId
    final ApplicationLinkService applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService.class)
    log.info "OTD-ALLOCATION. CONFLUENCE LINK. APPLICATION LINK SERVICE GOT"

    ApplicationLink confLink = null
    try {
        confLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class)
    } catch (Exception e) {
        log.error "OTD-ALLOCATION. CONFLUENCE LINK. ERROR WHILE GETTING CONFLUENCE PRIMARY LINK: " + e.toString()
        return
    }
    log.info "OTD-ALLOCATION. CONFLUENCE LINK. PRIMARY LINK " + (confLink == null ? "IS NULL" : "GOT")

    final List<String> forTitle = new ArrayList()
    try {
        def applicationLinkRequestFactory = confLink.createAuthenticatedRequestFactory()
        log.info "OTD-ALLOCATION. CONFLUENCE LINK. REQUEST FACTORY GOT"

        def request = applicationLinkRequestFactory.createRequest(Request.MethodType.GET, confRestUrl)
        log.info "OTD-ALLOCATION. CONFLUENCE LINK. REQUEST GOT"

        request.execute(new ResponseHandler<Response>() {
            @Override
            void handle(Response response) throws ResponseException {
                log.info "OTD-ALLOCATION. CONFLUENCE LINK. REQUEST HANDLING. STATUS CODE: " + response.getStatusCode()
                try {
                    byte[] bytes = new byte[10 * 1024];
                    response.getResponseBodyAsStream().read(bytes);
                    String readString = new String(bytes, "UTF-8").trim()
                    Node list = new XmlParser().parseText(readString)
                    forTitle.add(((NodeList) list.get("title")).text())
                } catch (Exception e) {
                    log.error "OTD-ALLOCATION. CONFLUENCE LINK. ERROR WHILE HANDLING REQUEST: " + e.toString()
                    return
                }
                log.info "OTD-ALLOCATION. CONFLUENCE LINK. PAGE TITLE: " + forTitle.get(0)
            }
        })
    } catch (Exception e) {
        log.error "OTD-ALLOCATION. CONFLUENCE LINK. ERROR WHILE CREATING AND EXECUTING REQUEST: " + e.toString()
        return
    }

    log.info "OTD-ALLOCATION. CONFLUENCE LINK. START TO BUILD REMOTE LINK"
    RemoteIssueLinkBuilder remoteIssueLinkBuilder = new RemoteIssueLinkBuilder();
    remoteIssueLinkBuilder.issueId(issue.getId());
    remoteIssueLinkBuilder.relationship("Wiki Page");
    remoteIssueLinkBuilder.iconUrl("https://conf.inttrust.ru:8443/images/icons/favicon.png");
    remoteIssueLinkBuilder.title(forTitle.isEmpty() ? "ERROR WHILE GETTING PAGE TITLE" : (String) forTitle.get(0));
    remoteIssueLinkBuilder.url(confUrl);
    remoteIssueLinkBuilder.applicationType(RemoteIssueLink.APPLICATION_TYPE_CONFLUENCE)
    remoteIssueLinkBuilder.applicationName("InterTrust Confluence")
    remoteIssueLinkBuilder.iconTitle("InterTrust Confluence")
    String globalId = confUrl.contains("halo-hp") ? "df0c5cf1-0a1f-37a7-9977-c139567a593e" : "5aded303-2b15-32d2-b67b-aa1ef31738b3"
    remoteIssueLinkBuilder.globalId("appId=" + globalId + "&pageId=" + confPageId)
    RemoteIssueLink remoteIssueLink = remoteIssueLinkBuilder.build();
    log.info "OTD-ALLOCATION. CONFLUENCE LINK. REMOTE LINK BUILT"

    try {
        RemoteIssueLinkService remoteIssueLinkService = ComponentManager.getComponentInstanceOfType(RemoteIssueLinkService.class)
        RemoteIssueLinkService.CreateValidationResult createValidationResult =
                remoteIssueLinkService.validateCreate(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), remoteIssueLink)
        remoteIssueLinkService.create(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser(), createValidationResult)
    } catch (Exception e) {
        log.error "OTD-ALLOCATION. CONFLUENCE LINK. ERROR WHILE CREATING REMOTE LINK: " + e.toString()
        return
    }

    log.info "OTD-ALLOCATION. CONFLUENCE LINK. REMOTE LINK CREATED"
}

log.info "__________OTD-ALLOCATION: POSTFUNCTION COMPLETED__________"
