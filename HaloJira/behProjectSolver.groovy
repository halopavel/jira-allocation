String fieldAuto = "Проявляется в"
String fieldManual = "Проявляется в версии"
String chosenProject = getFieldByName("Проект JIRA").getValue().toString()
String issueProject = getIssueContext().getProjectObject().getId().toString()

if (issueProject.equals(chosenProject)) {
    getFieldByName(fieldAuto).setHidden(false)
    getFieldByName(fieldManual).setHidden(true)
}
else {
    getFieldByName(fieldManual).setHidden(false)
    getFieldByName(fieldAuto).setHidden(true)
}
