import org.apache.log4j.Logger
import org.apache.log4j.Level

log = Logger.getLogger("com.acme.CreateSubtask")
log.setLevel(Level.DEBUG)

def field = getFieldByName("Проект JIRA")
def value = field.getValue()
log.debug("OTD_Allocation - behaviours. JIRA Project current:" + field)

if (value == null || "-1".equals(value)) {
    String newValue = issueContext.projectObject.id
    field.setFormValue(newValue)
    log.debug("OTD_Allocation - behaviours. JIRA Project set: " + newValue)

    String fieldAuto = "Проявляется в"
    String fieldManual = "Проявляется в версии"
    String issueProject = getIssueContext().getProjectObject().getId().toString()

    if (issueProject.equals(newValue)) {
        getFieldByName(fieldAuto).setHidden(false)
        getFieldByName(fieldManual).setHidden(true)
    }
    else {
        getFieldByName(fieldManual).setHidden(false)
        getFieldByName(fieldAuto).setHidden(true)
    }
}