import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.fields.layout.field.FieldLayout
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.util.IssueChangeHolder
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem
import com.atlassian.jira.security.roles.DefaultRoleActors
import com.atlassian.jira.security.roles.ProjectRoleManager
import com.atlassian.jira.user.ApplicationUser
import org.apache.log4j.Logger
import org.apache.log4j.Level

log = Logger.getLogger("com.acme.CreateSubtask")
log.setLevel(Level.DEBUG)

log.debug "__________ROLE-MEMBERS: STARTED_____________"

// Параметры:
def projectRoleId = 10001  // ID of Automatic Watcher project role
def customFieldName = "PAVEL" // название изменяемого поля

def projectRoleManager = ComponentAccessor.getComponent(ProjectRoleManager)

def role = projectRoleManager.getProjectRole(projectRoleId)
log.debug "[ROLE-MEMBERS] role with id " + projectRoleId + " is '" + role + "'"

DefaultRoleActors actors = projectRoleManager.getProjectRoleActors(role, issue.projectObject)
log.debug "[ROLE-MEMBERS] actors: " + actors
if (actors == null)
    return

Set<ApplicationUser> members = actors.getApplicationUsers()
ApplicationUser roleMember = (members == null || !members.iterator().hasNext()) ? null : members.iterator().next();
log.debug "[ROLE-MEMBERS] chosen actor: " + roleMember
if (roleMember == null)
    return

CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
Issue anIssue = issue
CustomField cf = customFieldManager.getCustomFieldObjectByName(customFieldName);
log.debug "[ROLE-MEMBERS] custom field: " + cf

FieldLayoutManager fieldLayoutManager = ComponentManager.getInstance().getFieldLayoutManager();
FieldLayout layout = fieldLayoutManager.getFieldLayout(
        issue.getProjectObject().getGenericValue(),
        issue.getIssueTypeObject().getId()
)
if (layout.getId() == null) {
    layout = fieldLayoutManager.getEditableDefaultFieldLayout();
}
FieldLayoutItem flo = layout.getFieldLayoutItem(cf.getId());

ModifiedValue modifiedValue = new ModifiedValue(anIssue.getCustomFieldValue(cf), roleMember);
IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();
cf.updateValue(flo, anIssue, modifiedValue, changeHolder);

log.debug "__________ROLE-MEMBERS: FINISHED___________"
