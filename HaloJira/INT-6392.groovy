import java.text.SimpleDateFormat

final String dateTimePattern = "dd/MM/yy h:mm a"

final int BLOCKER  = 12;
final int CRITICAL = 40;
final int MAJOR    = 80;
final int MINOR    = 160;
final int TRIVIAL  = 320;

final int DAY_IN_HOURS = 8;

final String BLOCKER_STR  = "Blocker";
final String CRITICAL_STR = "Critical";
final String MAJOR_STR    = "Major";
final String MINOR_STR    = "Minor";
final String TRIVIAL_STR  = "Trivial";

def dtField = getFieldByName("Срок исполнения ЦР")
String value = dtField.getValue()

if ((value == null || "".equals(value)) && "Принять".equals(getActionName())) {
	def priority = underlyingIssue.getPriority().get("name")

	if (priority == null)
		return;

	Calendar cal = null;
	if(priority.equals(BLOCKER_STR)) {
		cal = getDate(DAY_IN_HOURS, BLOCKER);
	} else if(priority.equals(CRITICAL_STR)) {
		cal = getDate(DAY_IN_HOURS, CRITICAL);
	} else if(priority.equals(MAJOR_STR)) {
		cal = getDate(DAY_IN_HOURS, MAJOR);
	} else if(priority.equals(MINOR_STR)) {
		cal = getDate(DAY_IN_HOURS, MINOR);
	} else if(priority.equals(TRIVIAL_STR)) {
		cal = getDate(DAY_IN_HOURS, TRIVIAL);
	} else {
		cal = getDate(DAY_IN_HOURS, 0);
	}

	SimpleDateFormat dateFormat = new SimpleDateFormat(dateTimePattern);
	dtField.setFormValue(dateFormat.format(new Date(cal.getTimeInMillis())));
}

Calendar getDate(dayInHours, roll) {
	if(dayInHours < 0 || roll < 0)
		throw new IllegalArgumentException();


	final int HOUR_OF_START = 10;
	final Calendar cal = Calendar.getInstance();
	// Если после HOUR_OF_START часов, то переносим на следующий день
	final int c = roll / dayInHours + (cal.get(Calendar.HOUR_OF_DAY) > HOUR_OF_START - 1 ? 0 : -1);


	int i = 0;
	while(i < c || (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
			cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
		cal.add(Calendar.DATE, 1);
		if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY ||
				cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			--i;
		}
		++i;
	}
	cal.add(Calendar.HOUR_OF_DAY, roll % dayInHours)
	return cal;
}