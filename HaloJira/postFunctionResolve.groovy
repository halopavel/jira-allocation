import java.sql.Timestamp;
import java.util.Date

import com.atlassian.jira.ComponentManager; // Main Jira's block
import com.atlassian.jira.issue.Issue; // For work with Jira's Issues
import com.atlassian.jira.issue.IssueManager; // Issue manager wich help get params or fields(get methods)
import com.atlassian.jira.issue.MutableIssue; // Kind of issue for set methods
import com.atlassian.jira.issue.ModifiedValue; // set/get methods for fields values
//
import com.atlassian.jira.issue.fields.FieldManager; // get methods
import com.atlassian.jira.issue.CustomFieldManager; // 	some methods for custom fields
import com.atlassian.jira.issue.fields.CustomField; // 	get methods for custom fields
import com.atlassian.jira.issue.customfields.CustomFieldType; // somthing
//
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder; // something
import com.atlassian.jira.issue.util.IssueChangeHolder; // uses for change fields by Issue id

// Variables for customFieldManager's handler
def tgtFieldA, changeHolderA;
// Debug string which use for writing exeptions or other stuff
def string = "";

int day_starts = 10, day_ends = 18, day_length = day_ends - day_starts;
int break_point = 6;
if(break_point == 0)
    break_point = day_length;
int add_days = 0, days_passed = 0, work_days = 0, days = 0;
int weeks = 0, weekends = 0;
int hours_passed = 0, hours = 0;
int quested = 0, answered = 0;
int isweekend = 0;

MutableIssue mutableissue 		= (MutableIssue) issue;
MutableIssue myIssue 			= issue;
Timestamp due_date 				= mutableissue.getDueDate();
Timestamp updated 				= mutableissue.getUpdated();
Timestamp created 				= mutableissue.getCreated();

CustomFieldManager customFieldManager = ComponentManager.getInstance().getCustomFieldManager();
Issue issue  = issue;


//tgtFieldA 		= customFieldManager.getCustomFieldObject("Срок исполнения ЦР");
changeHolderA 	= new DefaultIssueChangeHolder();
//def old_v 		= issue.getCustomFieldValue(tgtFieldA);
def old_v = new Timestamp(new Date().getTime() - 100000000)
// INT-6668
def crDueDateField = customFieldManager.getCustomFieldObjectByName("Срок исполнения ЦР");
Timestamp cr_due_date = issue.getCustomFieldValue(crDueDateField);

Date DueDate 		= new Date(due_date.getTime());
Date CrDueDate      = new Date(cr_due_date.getTime());

Date current_day 	= new Date();
Date quested_day 	= new Date(old_v.getTime());

quested 	= old_v.getHours();
answered 	= current_day.getHours();

if(quested_day.getDay() == 6) {
    string += "\nAsked on weekend: add 2";
    quested_day = quested_day.plus(2);
    old_v 		= old_v.plus(2);
}
if(quested_day.getDay() == 0) {
    string += "\nAsked on weekend: add 1";
    quested_day = quested_day.plus(1);
    old_v 		= old_v.plus(1);
}

if(current_day.getDay() == 6) {
    string += "\nAsked on weekend: add 2";
    current_day = current_day.plus(2);
}
if(current_day.getDay() == 0) {
    string += "\nAsked on weekend: add 1";
    current_day = current_day.plus(1);
}

if(quested == 0) {
    string += "\nExeption";
    quested = 12;
}
string += "\nQuested: " + quested;
string += "\nAnswered: " + answered;


days_passed = current_day.minus(old_v);

int currHours = DueDate.getHours();
int currHoursCr = CrDueDate.getHours();
currHours = currHours + 1;
currHoursCr = currHoursCr + 1;
DueDate.setHours(currHours);
CrDueDate.setHours(currHoursCr);

string += "\nDays passed: " + days_passed;
string += "\nQuested day: " + quested_day;
string += "\nQuested day as week: " + quested_day.getDay();
string += "\nOld DueDate: " + DueDate;

if(days_passed) {
    if(quested < day_starts)
        quested = day_starts;
    if(quested > day_ends) {
        quested = day_starts;
        string += "\nquested > day_ends: adds 1 day";
    }
    if(answered < day_starts)
        answered = day_starts;
    if(answered > day_ends) {
        answered = day_starts;
        string += "\nanswered > day_ends: adds 1 day";
    }

    // 15 - 10 = 5
    quested = quested - day_starts;
    // 18 - 15 = 3
    answered = day_ends - answered;
    string += "\nFrom start: " + quested;
    string += "\nFrom end: " + answered;
    hours_passed += quested + answered;
    hours_passed -= day_length;

    // If passed 1 or more days, work hours can't let us add 1 work day,
    // we minus 1 day
    /*if(hours_passed < break_point) {
    if(days_passed) {
        days_passed -= 1;
        }
    }*/

    if(hours) {
        hours_passed += hours_passed + hours;
    }
    if(hours) {
        hours_passed = hours_passed + hours;
    }
    if(hours_passed >= break_point) {
        add_days = add_days + 1;
        string += "\nHours > Break point adds 1 day";
    }
} else {
    hours_passed = answered - quested;
    if(hours_passed >= break_point) {
        add_days = add_days + 1;
        string += "\nHours > Break point adds 1 day";
    }
}

string += "\nQuested: " + quested + " Answered: " + answered;

Date addWorkDays(Date date, int workDays) {
    int day = date.getDay();
    int currentDayOfWeek;
    int addDays = 0;
    int weeks = 0;
    int days = 0;

    if (workDays > 5) {
        weeks = workDays / 5;
        addDays = weeks * 7;
        days = workDays - weeks * 5;
    } else if (workDays == 5) {
        days = workDays + 2;
    } else {
        days = workDays;
    }

    if(day == 4 || day == 5) {
        if(days == 4)
            days = days + 2;
    }
    if(day == 5) {
        if(days == 3)
            days = days + 2;
    }

    days = days + addDays;

    date = date.plus(days);
    if(date.getDay() == 6) {
        date = date.plus(2);
    } else if(date.getDay() == 0) {
        date = date.plus(2);
    }

    return date;
}

if(days_passed) {
    if(days_passed > 7) {
        weeks = days_passed / 7;
        weekends = weeks * 2;
    } else if(days_passed == 6) {
        weeks = 1;
        weekends = weeks * 2;
    } else {
        int day = 0;
        for(int x = 1; x <= days_passed; x++) {
            quested_day = quested_day.plus(1);
            day = quested_day.getDay();
            if(day == 6 || day == 0) {
                weekends = 2;
            }
        }
    }
    work_days = days_passed - weekends;
    string += "\nWork days: " + work_days;
    string += "\nWeeks: " + weeks;
    string += "\nWeek ends: " + weekends;
    if(add_days) {
        work_days = work_days + add_days;
    }
    DueDate = addWorkDays(DueDate, work_days);
    CrDueDate = addWorkDays(CrDueDate, work_days);
} else {
    if(add_days) {
        days_passed = days_passed + add_days;
        DueDate = addWorkDays(DueDate, days_passed);
        CrDueDate = addWorkDays(CrDueDate, days_passed);
    }
}

int day = DueDate.getDay();

if(day == 6) {
    DueDate = DueDate.plus(2);
}
if(day == 0) {
    DueDate = DueDate.plus(1);
}

string += "\nHours passed: " + hours_passed;
string += "\nAdd Days: " + add_days;

string += "\nNew DueDate: " + quested_day;
string += "\nNew DueDate as week: " + quested_day.getDay();

due_date = new Timestamp(DueDate.getTime());
myIssue.setDueDate(due_date);
string += "\nUpdated: " + updated;


int dayCr = CrDueDate.getDay();

if(dayCr == 6) {
    CrDueDate = CrDueDate.plus(2);
}
if(dayCr == 0) {
    CrDueDate = CrDueDate.plus(1);
}
cr_due_date = new Timestamp(CrDueDate.getTime());
crDueDateField.updateValue(
        null, mutableissue, new ModifiedValue(issue.getCustomFieldValue(crDueDateField), cr_due_date), changeHolderA);

log.info "COMPLETED! YEP!"

//myIssue.setDescription(string);
/*
def tgtField = customFieldManager.getCustomFieldObject(11613);
def changeHolder = new DefaultIssueChangeHolder();
tgtField.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(tgtField), string),changeHolder);
*/